---
title: "GitLab R Markdown Learning"
author: "Julia Hage"
date: "5/3/2022"
output: 
  html_document: 
    keep_md: yes
---


The graph below shows U.S. population growth over the period of 1800 to 1960. 

![](RMarkdownLearning_files/figure-html/unnamed-chunk-3-1.png)<!-- -->
